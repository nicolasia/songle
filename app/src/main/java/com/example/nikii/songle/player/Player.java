package com.example.nikii.songle.player;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.nikii.songle.utils.Common;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by nikii on 30/11/2017.
 */

public class Player {
    private Marker playerMarker;
    private GoogleMap mMap;
    private Resources res;

    public Player(GoogleMap mMap, Resources res) {
        this.mMap = mMap;
        this.res = res;
    }

    public LatLng getPlayerPosition(){
        return playerMarker.getPosition();
    }

    public void addIcon(int id, int width, int height, LatLng latLng){
        playerMarker = mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons(id, width, height))));
        playerMarker.setTag(Common.MarkerTypes.Player);
        mMap.getUiSettings().setMapToolbarEnabled(false);
    }

    private Bitmap resizeMapIcons(int id, int width, int height){
        Bitmap imageBitmap = BitmapFactory.decodeResource(res, id);
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, width, height, false);
        return resizedBitmap;
    }

    public void moveTo(LatLng position){
        if (playerMarker != null) {
            MarkerAnimation.animateMarkerToICS(playerMarker, position, new LatLngInterpolator.Linear());
        }
    }

}
