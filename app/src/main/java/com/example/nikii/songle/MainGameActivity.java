package com.example.nikii.songle;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nikii.songle.location.services.LocationUpdater;
import com.example.nikii.songle.lyrics.parser.Line;
import com.example.nikii.songle.lyrics.parser.Lyrics;
import com.example.nikii.songle.placemark.parser.Placemark;
import com.example.nikii.songle.placemark.parser.WordPosition;
import com.example.nikii.songle.style.parser.Style;
import com.example.nikii.songle.player.Player;
import com.example.nikii.songle.songs.parser.Song;
import com.example.nikii.songle.utils.Common;
import com.example.nikii.songle.utils.LevenshteinComparison;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */

public class MainGameActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener,
        GoogleMap.OnMarkerClickListener, LocationUpdater.OnLocationUpdatedListener{

    private GoogleMap mMap;
    private Player player;
    private Map<Marker, Placemark> markerToPlacemark;
    private String userGuess;
    private LocationUpdater locationUpdater;
    private Boolean hasFoundWord;
    private int guessedWrong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_game);

        //generates the layout for the review words page
        markerToPlacemark = new HashMap<>();
        generateCollectable();


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        locationUpdater = new LocationUpdater(this);
        locationUpdater.initLocationApi();
        locationUpdater.setOnLocationUpdateListener(this);
        hasFoundWord = false;
        guessedWrong = 0;
    }

    @Override
    public void onLocationUpdated(Location location) {
        player.moveTo(new LatLng(location.getLatitude(), location.getLongitude()));
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(this);
        mMap.setOnMarkerClickListener(this);

        // Add a marker in Sydney and move the camera
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this,"turn on GPS!", Toast.LENGTH_LONG).show();
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        //the centre of the boundaries specified. The map will automatically zoom there when loaded
        LatLng mapCenter = new LatLng(55.944425, -3.188396);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mapCenter, 16));
        player = new Player(mMap, getResources());
        LatLng latLng = new LatLng(55.938547, -3.180809);
        player.addIcon(R.drawable.go, 140, 140, latLng);

//        LatLng mapCenter = new LatLng(55.944425, -3.188396);
     //   mMap.setMyLocationEnabled(true);
     //   mMap.getUiSettings().setMyLocationButtonEnabled(true);
//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mapCenter, 16));

        populateMap();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //stops the location updates when app is paused
        locationUpdater.stopLocationUpdates();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //gets location updates when the app is currently in use
        locationUpdater.startLocationUpdates();
    }


    public void moveToPlayer(View view){
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(player.getPlayerPosition(), 15));
    }

    public void showHint(View view){
        openSongsDialog();

    }

    public void onClickToHomePage(View view){
        Intent intent = new Intent(this,HomePageActivity.class);
        startActivity(intent);
    }

    //This generates the hint, ie. the line of the last word found
    public void getLineOfWord(View view){
        GamePlayData data = GamePlayData.getInstance();

        if(hasFoundWord) {
            Line lyricLine = data.lyrics.getLine(data.lastWordFoundIndex - 1);
            Log.e("LastWordFoundIndex", data.lastWordFoundIndex - 1 + "");
            Log.e("LastWordFoundIndex", data.lyrics.getLine(data.lastWordFoundIndex - 1) + "");
            Toast.makeText(this, lyricLine.toString(), Toast.LENGTH_SHORT).show();
        } else{
            Toast.makeText(this,"Can't generate hint until a word has been found", Toast.LENGTH_SHORT).show();
        }

    }


    //Generates the correct placemarks and style on the map in the correct locations with corresponding word
    private void populateMap(){

        GamePlayData gameData = GamePlayData.getInstance();
        Song currSong = gameData.currentSong;
        Map<String, Bitmap> stylesMap = new HashMap<>();
        Log.e("MainGameActivity", "index=" +currSong.number + " songName=" + currSong.title);
        for (Style style: gameData.songMap.getStyles()){
            stylesMap.put(style.id, style.getBitmap());
        }
        for (Placemark placemark: gameData.songMap.getPlacemarks()){
            Bitmap markerBitmap = stylesMap.get(placemark.styleURL.substring(1));

            int row = placemark.wordPosition.getRow() - 1;
            int column = placemark.wordPosition.getColumn() - 1;
            String word = gameData.lyrics.getWord(row, column);
            placemark.wordPosition.setWord(word);

//            Log.e("MainGameActivity", "row=" + row + " column=" + column);
//            Log.e("MainGameActivity", "Line=" + gameData.lyrics.getLine(row));

            Marker marker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(placemark.coord.latitude, placemark.coord.longitude))
                    .icon(BitmapDescriptorFactory.fromBitmap(markerBitmap)));
            // make the marker of type placemark
            marker.setTag(Common.MarkerTypes.Placemark);


            markerToPlacemark.put(marker, placemark);
        }
       // Log.e("Song", gameData.currentSong.title);
    }

    @Override
    public void onMapClick(LatLng latLng) {
       // Toast.makeText(this, mMap.getMyLocation().getLatitude() + "",Toast.LENGTH_LONG).show();

     //   player.moveTo(latLng);

    }

    private Lyrics collectable;

    private void generateCollectable() {
        collectable = GamePlayData.getInstance().lyrics.createCollectable();
       // for (Line line: collectable.getLines()){
       // }
    }



    //Checks if user can collect the placemark, if they can then remove that placemark and add the word to the review page
    @Override
    public boolean onMarkerClick(Marker marker) {

        GamePlayData data = GamePlayData.getInstance();

        if (Common.MarkerTypes.Placemark.equals(marker.getTag())){

            Placemark placemark = markerToPlacemark.get(marker);
            double distanceBetweenPoints = getDistanceBetweenPoints(player.getPlayerPosition(),marker.getPosition());

            if(distanceBetweenPoints < 20){
                Log.e("Distance", distanceBetweenPoints + "");
                Toast.makeText(this, "You found \"" + placemark.wordPosition.getWord() + "\"", Toast.LENGTH_SHORT).show();
                placemark.setFound(true);
                data.lastWordFoundIndex = placemark.wordPosition.getRow();
                marker.remove();
                markerToPlacemark.remove(marker);

                collectable.setWord(placemark.wordPosition.getRow()-1,
                        placemark.wordPosition.getColumn()-1,
                        placemark.wordPosition.getWord());

                hasFoundWord = true;

                return true;
            }

            marker.setTitle((Math.ceil(distanceBetweenPoints - 20)) + "m away");

        }

        return false;
    }

    //algorithm for getting distance between two points
    private double getDistanceBetweenPoints(LatLng latLngA, LatLng latLngB){
        Location locationA = new Location("point A");
        locationA.setLatitude(latLngA.latitude);
        locationA.setLongitude(latLngA.longitude);
        Location locationB = new Location("point B");
        locationB.setLatitude(latLngB.latitude);
        locationB.setLongitude(latLngB.longitude);

       return locationA.distanceTo(locationB);
    }

    private void openSongsDialog(){

        AlertDialog.Builder builder = new AlertDialog.Builder(MainGameActivity.this);

        View view = getLayoutInflater().inflate(R.layout.songs_dialog, null);
        TextView songLyrics = view.findViewById(R.id.songLyrics);

        songLyrics.setText(collectable.toString());

        builder.setView(view);
        final AlertDialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();
        ImageButton backToGame = dialog.findViewById(R.id.backButton);

        backToGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    //this gets the input of the users guess in an alert dialogue
    public void getUserGuess(View view){
        final Context context = this;
        //String input = "";

        //result = (EditText) findViewById(R.id.editTextDialogUserInput);
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.user_input, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptsView);
        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.editTextDialogUserInput);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // get user input and set it to result
                                userGuess = userInput.getText().toString();
                                //checkAnswerSimilarity();
                                checkAnswer();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        // show it
        alertDialog.show();

       // return userInput.getText().toString();

    }

    //Check if user has tried more than 5 times in guessing the song. If so, they can give up and proceed to next level
    public void onUserGiveUp(View view){

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to give up?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(guessedWrong >= 5) {
                            showSong();
                            nextLevel();
                        } else{
                            showMoreTries();
                        }
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        builder.create();
        builder.show();

    }

    private void showMoreTries(){
        Toast.makeText(this,"Give it at least " + (5 - guessedWrong) + " tries!", Toast.LENGTH_SHORT).show();
    }

    private void showSong(){
        GamePlayData data = GamePlayData.getInstance();

        Toast.makeText(this,"The song was " + data.currentSong.title,Toast.LENGTH_SHORT).show();
    }

    //Checks the similarity between the answer and the users guess
    private int checkAnswerSimilarity(){
        GamePlayData gameData = GamePlayData.getInstance();
        int similarity = new LevenshteinComparison().distance(gameData.currentSong.title,userGuess);
        return similarity;
    }

    private void checkAnswer(){

        GamePlayData data = GamePlayData.getInstance();
        int threshold = userGuess.length() - 3;
        int similarity = checkAnswerSimilarity();
        if(similarity <= threshold){
            Log.e("similarity", similarity + "help");
            Toast.makeText(this, "Correct answer!", Toast.LENGTH_LONG).show();
            nextLevel();
            //storeCorrectSong();
            saveData();
        }
        else{
            Toast.makeText(this, "Wrong answer",Toast.LENGTH_LONG).show();
            guessedWrong++;
        }
    }

    //saves the song if correct or gave up into SharedPreferences
    public void saveData(){

        GamePlayData gameData = GamePlayData.getInstance();


        SharedPreferences.Editor editor = getSharedPreferences("Song", MODE_PRIVATE).edit();

        editor.putInt(gameData.currentSong.title, gameData.currentSong.number);
        editor.apply();

      //  Log.e("SongID",gameData.currentSong.number + "");
      //  retrieveData();

    }


    //Asks the user if they wish to go to the next level
    private void nextLevel(){
        final Context context = this;
        final ImageButton nextLevel = (ImageButton)findViewById(R.id.nextLevel);
        //String input = "";

        //result = (EditText) findViewById(R.id.editTextDialogUserInput);
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.user_input, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you wish to proceed to the next level?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                       // saveData();
                        System.out.println("proceeding");
                        //finish();
                        loadNextMap();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        nextLevel.setVisibility(View.VISIBLE);
                    }
                });
        builder.create();
        builder.show();
    }

    public final static String CURRENT_LEVEL = "CURRENT_LEVEL";

    //Reloads the splash screen two to go to the next level
    public void loadNextMap(){
        GamePlayData data = GamePlayData.getInstance();

        int level = getIntent().getIntExtra(SplashScreenTwo.MAP_LEVEL,0);
        data.currentLevel = level;
        Log.e("MAP LEVEL", level + "");

        Intent intent = new Intent(this, SplashScreenTwo.class);
        intent.putExtra(CURRENT_LEVEL,true);
      //  Log.e("intent data", retrieveData() + "");
        startActivity(intent);
    }

    public void onClickNextLevel(View view){
        loadNextMap();
    }

}
