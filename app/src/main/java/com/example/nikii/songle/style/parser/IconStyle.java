package com.example.nikii.songle.style.parser;

/**
 * Created by nikii on 15/11/2017.
 */

public class IconStyle {
    public final double scale;
    public final Icon icon;

    public IconStyle(double scale, Icon icon) {
        this.scale = scale;
        this.icon = icon;
    }
}
