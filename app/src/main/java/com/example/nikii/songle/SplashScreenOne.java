package com.example.nikii.songle;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.example.nikii.songle.songs.parser.DownloadSongsXml;
import com.example.nikii.songle.songs.parser.Song;
import com.example.nikii.songle.songs.parser.Songs;

public class SplashScreenOne extends Activity implements DownloadSongsXml.AsyncSongsDownloader{

    /** Duration of wait **/
    private final int SPLASH_DISPLAY_LENGTH = 3000;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_splashscreen);

        /* New Handler to start the Menu-Activity 
         * and close this Splash-Screen after some seconds.*/
        if(isNetworkConnected()) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                    Intent mainIntent = new Intent(SplashScreenOne.this, HomePageActivity.class);
                    startActivity(mainIntent);
                    finish();
                }
            }, SPLASH_DISPLAY_LENGTH);
        } else{
            Toast.makeText(this, "You need to connect to the internet! Please retry", Toast.LENGTH_LONG).show();
            finish();

        }
    }



    @Override
    protected void onResume() {
        super.onResume();
        if(isNetworkConnected()){
            downloadSongs();
        } else{

        }

    }

    //Checks if the device is connected to the internet. If not, close the app
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    //downloads the data of Songs
    public void downloadSongs(){
        String xmllink = "http://www.inf.ed.ac.uk/teaching/courses/selp/data/songs/songs.xml";
        new DownloadSongsXml(this).execute(xmllink);
    }

    //Stores the songs in the singleton
    @Override
    public void onXmlDownloaded(Songs songs) {
        for ( Song s: songs.getSongs()){
            System.out.println(s.title);
        }

        GamePlayData data = GamePlayData.getInstance();
        data.songs = songs;
    }



}