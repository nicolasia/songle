package com.example.nikii.songle.placemark.parser;

import com.example.nikii.songle.utils.CommonXmlParser;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created by nikii on 03/11/2017.
 */

public class PlacemarkXmlParser {

    private static final String ns = null;

    public Placemark readPlacemark(XmlPullParser parser) throws XmlPullParserException,IOException {

        parser.require(XmlPullParser.START_TAG, ns, "Placemark");
        WordPosition wordPosition = null;
        String description = null;
        String styleURL = null;
        Coordinate coord = null;

        while (parser.next() != XmlPullParser.END_TAG) {

            String name = parser.getName();

            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }

            if (name.equals("name")) {
                wordPosition = readName(parser);
            } else if (name.equals("description")) {
                description = readDesc(parser);
            } else if (name.equals("styleUrl")) {
                styleURL = readStyleUrl(parser);
            } else if (name.equals("Point")) {
                coord = readPoint(parser);
            } else {
                CommonXmlParser.skip(parser);
            }

        }
        return new Placemark(wordPosition, description, styleURL, coord);
    }


    private Coordinate readPoint(XmlPullParser parser) throws IOException,XmlPullParserException {

        parser.require(XmlPullParser.START_TAG,ns,"Point");
        Coordinate coord = null;
        while(parser.next() != XmlPullParser.END_TAG) {
            String name = parser.getName();
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            if (name.equals("coordinates")) {
                coord = readCoord(parser);
            } else{
                CommonXmlParser.skip(parser);
            }
        }

        return coord;


    }
    private WordPosition readName(XmlPullParser parser) throws IOException, XmlPullParserException{

        WordPosition wordPosition = null;
        parser.require(XmlPullParser.START_TAG,ns,"name");
        String[] wordList = CommonXmlParser.readText(parser).split(":");
        try {
            wordPosition = new WordPosition(
                    Integer.parseInt(wordList[0]),
                    Integer.parseInt(wordList[1]));
        } catch (NumberFormatException e){
            e.printStackTrace();
        }
        parser.require(XmlPullParser.END_TAG,ns,"name");
        return wordPosition;
    }

    private String readDesc(XmlPullParser parser) throws IOException, XmlPullParserException{

        parser.require(XmlPullParser.START_TAG,ns,"description");
        String description = CommonXmlParser.readText(parser);
        parser.require(XmlPullParser.END_TAG,ns,"description");
        return description;
    }

    private String readStyleUrl(XmlPullParser parser) throws IOException, XmlPullParserException{

        parser.require(XmlPullParser.START_TAG,ns,"styleUrl");
        String styleURL = CommonXmlParser.readText(parser);
        parser.require(XmlPullParser.END_TAG,ns,"styleUrl");
        return styleURL;
    }

    private Coordinate readCoord(XmlPullParser parser) throws IOException, XmlPullParserException{
        //This splits the string read in the kml file to get the required Longitude, Latitude and Altitude
        Coordinate coord = null;
        parser.require(XmlPullParser.START_TAG,ns,"coordinates");
        String[] coordList = CommonXmlParser.readText(parser).split(",");
        try {
            coord = new Coordinate(
                    Double.parseDouble(coordList[0]),
                    Double.parseDouble(coordList[1]),
                    Double.parseDouble(coordList[2]));
        } catch (NumberFormatException e){
            e.printStackTrace();
        }
        parser.require(XmlPullParser.END_TAG,ns,"coordinates");
        return coord;
    }
}
