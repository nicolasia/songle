package com.example.nikii.songle.location.services;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.example.nikii.songle.MainGameActivity;
import com.example.nikii.songle.player.Player;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import static android.Manifest.permission_group.LOCATION;
import static android.os.FileObserver.ACCESS;
import static java.util.logging.Level.FINE;


/**
 * Created by nicole on 10/12/2017.
 */

public class LocationUpdater implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private GoogleApiClient mLocationClient;
    private Location mCurrentLocation;
    private LocationRequest mLocationRequest;
    private Context context;
    private OnLocationUpdatedListener listener;


    public LocationUpdater(Context context) {
        this.context = context;
    }

    //Checks that the location has been updated
    public void setOnLocationUpdateListener(OnLocationUpdatedListener listener) {
        this.listener = listener;
    }

    //Constructor
    public void initLocationApi() {
        mLocationClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setFastestInterval(5000);
    }

    public void startLocationUpdates() {
        mLocationClient.connect();
    }

    public void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mLocationClient, this);

        mLocationClient.disconnect();
    }

    //Updates Location
    @Override
    public void onLocationChanged(Location location) {

        mCurrentLocation = location;
        Log.d("Location Update", String.valueOf(mCurrentLocation.getLatitude()) +
                " " + String.valueOf(mCurrentLocation.getLongitude()));
        if (listener != null) {
            listener.onLocationUpdated(location);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)

    //Checks whether you have Internet or GPS turned on
    @Override
    public void onConnected(Bundle bundle) {
        // Display the connection status
        Toast.makeText(this.context, "Connected", Toast.LENGTH_SHORT).show();
        if (ActivityCompat.checkSelfPermission(this.context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this.context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this.context, "Please turn on GPS!", Toast.LENGTH_LONG).show();
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mLocationClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(this.context, "Disconnected. Please re-connect.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this.context, "Error occurred while connecting, please try again", Toast.LENGTH_SHORT).show();
    }

    private boolean isInternetConnected (){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        if (!isConnected){
            Toast.makeText(this.context, "Could not access the Server! Check Connectivity!", Toast.LENGTH_SHORT).show();
        }
        return isConnected;
    }

    public interface OnLocationUpdatedListener {
        void onLocationUpdated (Location location);
    }

}