package com.example.nikii.songle.lyrics.parser;

import android.os.AsyncTask;

import com.example.nikii.songle.utils.Common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by nicole on 16/11/2017.
 */

public class LyricsDownloader extends AsyncTask<String, Void, Lyrics> {
    private AsyncLyricsDownloader asyncLyricsDownloader;

    //Calls back to say the lyrics finished downloading
    public LyricsDownloader(AsyncLyricsDownloader asyncLyricsDownloader) {
        this.asyncLyricsDownloader = asyncLyricsDownloader;
    }

    @Override
    protected Lyrics doInBackground(String... urls) {
        Lyrics lyrics = null;
        try{
            lyrics = loadLyricsFromNetwork(urls[0]);
        } catch (IOException e){
            e.printStackTrace();
        }
        return lyrics;
    }

    @Override
    protected void onPostExecute(Lyrics lyrics){
        super.onPostExecute(lyrics);

        if (asyncLyricsDownloader != null){
            asyncLyricsDownloader.onLyricsDownloaded (lyrics);
        }
    }

    //Downloads the lyrics from online
    private Lyrics loadLyricsFromNetwork (String url) throws IOException {
        InputStream stream = null;
        Lyrics lyrics = new Lyrics();

        try{
            stream = Common.downloadUrl(url);

            BufferedReader in = new BufferedReader(new InputStreamReader(stream));
            String str;
            while ((str = in.readLine()) != null)
            {
                List<String> splitLine = new ArrayList<>(Arrays.asList(str.trim().split("\\s+")));
                int index = 0;
                try {
                    index = Integer.parseInt(splitLine.remove(0));
                } catch (NumberFormatException e){
                    e.printStackTrace();
                }

                lyrics.appendLine(new Line(splitLine));
            }

        } finally{
            if (stream != null){
                stream.close();
            }
        }
        return lyrics;
    }

    public interface AsyncLyricsDownloader {
        void onLyricsDownloaded (Lyrics lyrics);
    }
}