package com.example.nikii.songle.songs.parser;

/**
 * Created by nikii on 29/10/2017.
 */

public class Song {
    public final int number;
    public final String artist;
    public final String title;
    public final String link;

    //Constructor for Song
    public Song(int number, String artist, String title, String link) {
        this.number = number;
        this.artist = artist;
        this.title = title;
        this.link = link;
    }
}

  /*  public String getNumberString() {
        return numberString;
    }

    public String getArtist() {
        return artist;
    }

    public String getTitle(){
        return title;
    }

    public  String getLink(){
        return link;
    }
}
*/