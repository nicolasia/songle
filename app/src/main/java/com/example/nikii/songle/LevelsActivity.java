package com.example.nikii.songle;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class LevelsActivity extends AppCompatActivity {

    public final static String GAME_LEVEL = "GAME_LEVEL";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_levels_page);
    }

    public void onClickExit(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to exit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    public void onClick(DialogInterface dialog, int id) {
                        finishAndRemoveTask();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        builder.create();
        builder.show();

    }

    public void onClickHome(View view){

        Intent intent = new Intent(this, HomePageActivity.class);
        startActivity(intent);
    }

    //Passes the map level to the next splash screen to enable the correct download and generic the right link
    public void onClickToMap(View view){
        Intent intent = new Intent(this, SplashScreenTwo.class);
        switch (view.getId()){
            case R.id.noob:
                intent.putExtra(GAME_LEVEL,5);
                break;
            case R.id.easy:
                intent.putExtra(GAME_LEVEL,4);
                break;
            case R.id.intermediate:
                intent.putExtra(GAME_LEVEL,3);
                break;
            case R.id.hard:
                intent.putExtra(GAME_LEVEL,2);
                break;
            case R.id.overachiever:
                intent.putExtra(GAME_LEVEL,1);
                break;
        }
        startActivity(intent);

    }
}
