package com.example.nikii.songle.lyrics.parser;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nicole on 16/11/2017.
 */

public class Line {
    private List<String> line;

    //Constructor
    public Line(List<String> line) {
        this.line = new ArrayList<>(line);
    }

    //Copy constructor to create the layout for review words
    public Line(Line copy){
        this.line = new ArrayList<>(copy.getLines());
    }

    public List<String> getLines() {
        return line;
    }

    //Get a word in that line
    public String getWord(int index){
        return this.line.get(index);
    }

    public void setWord(int index, String word){
        this.line.set(index, word);
    }

    public int getLineLength(){
        return this.line.size();
    }

    @Override
    public String toString() {
        return TextUtils.join(" ", this.line);
    }
}
