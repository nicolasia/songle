package com.example.nikii.songle.songmap.parser;

import android.os.AsyncTask;

import com.example.nikii.songle.utils.Common;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by nikii on 05/11/2017.
 */

public class DownloadSongMapXml extends AsyncTask<String, Void, SongMap> {

    private AsyncSongMapDownloader asyncSongMapDownloader;

    //Checks that the Map of the song has been downloaded
    public DownloadSongMapXml(AsyncSongMapDownloader asyncSongMapDownloader) {
        this.asyncSongMapDownloader = asyncSongMapDownloader;
    }

    @Override
    protected SongMap doInBackground(String... urls){
        try{
            return loadXmlFromNetwork(urls[0]);
        } catch (IOException e){
            e.printStackTrace();
        } catch (XmlPullParserException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(SongMap result){
        super.onPostExecute(result);

        if (asyncSongMapDownloader != null){
            asyncSongMapDownloader.onSongMapDownloaded(result);
        }
    }

    //Downloads the data from online
    private SongMap loadXmlFromNetwork(String urlString) throws XmlPullParserException, IOException{
        InputStream stream = null;
        SongMap songMap = null;
        SongMapXmlParser songMapParser = new SongMapXmlParser();

        try {
            stream = Common.downloadUrl(urlString);
            songMap = songMapParser.parse(stream);
        } catch (Exception e){
            e.printStackTrace();
        } finally{
            if (stream != null){
                stream.close();
            }
        }

        return songMap;
    }

    public interface AsyncSongMapDownloader {
        void onSongMapDownloaded (SongMap songMap);
    }
}
