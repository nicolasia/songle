package com.example.nikii.songle.songs.parser;

import android.os.AsyncTask;

import com.example.nikii.songle.utils.Common;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by nikii on 01/11/2017.
 */

public class DownloadSongsXml extends AsyncTask<String, Void, Songs> {

    private AsyncSongsDownloader asyncSongsDownloader = null;

    //Calls back to make sure the songs have been downloaded
    public DownloadSongsXml(AsyncSongsDownloader asyncSongsDownloader) {
        this.asyncSongsDownloader = asyncSongsDownloader;
    }

    @Override
    protected Songs doInBackground(String... urls){
        try{
            return loadXmlFromNetwork(urls[0]);
        } catch (IOException e){
            e.printStackTrace();
        } catch (XmlPullParserException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Songs result){
        super.onPostExecute(result);
        if (asyncSongsDownloader != null){
            asyncSongsDownloader.onXmlDownloaded(result);
        }
    }

    private Songs loadXmlFromNetwork(String urlString) throws XmlPullParserException, IOException{
        InputStream stream = null;
        SongXmlParser songXmlParser = new SongXmlParser();
        List<Song> songs = null;

        try{
            stream = Common.downloadUrl(urlString);
            songs = songXmlParser.parse(stream);
        } finally{
            if (stream != null){
                stream.close();
            }
        }

        return new Songs(songs);
    }



    public interface AsyncSongsDownloader {
        void onXmlDownloaded(Songs songs);
    }
}
