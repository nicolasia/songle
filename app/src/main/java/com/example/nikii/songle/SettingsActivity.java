package com.example.nikii.songle;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
    }

    public void onClickToCompletedSongs(View view){
        Intent intent = new Intent(this, CompletedSongsActivity.class);
        startActivity(intent);
    }

    public void onClickHomeButton(View view){
        Intent intent = new Intent(this, HomePageActivity.class);
        startActivity(intent);
    }

    public void openAbout(View view){

        final Context context = this;
        //String input = "";

        //result = (EditText) findViewById(R.id.editTextDialogUserInput);
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.about_dialog, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptsView);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        // show it
        alertDialog.show();
    }

    public void openHowToPlay(View view){
        final Context context = this;
        //String input = "";

        //result = (EditText) findViewById(R.id.editTextDialogUserInput);
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.how_to_play_dialog, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptsView);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        // show it
        alertDialog.show();
    }
}
