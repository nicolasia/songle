package com.example.nikii.songle.songmap.parser;

import android.util.Xml;

import com.example.nikii.songle.placemark.parser.Placemark;
import com.example.nikii.songle.placemark.parser.PlacemarkXmlParser;
import com.example.nikii.songle.style.parser.Style;
import com.example.nikii.songle.style.parser.StyleXmlParser;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nikii on 15/11/2017.
 */

public class SongMapXmlParser {

    private static final String ns = null;

    public SongMap parse(InputStream in) throws XmlPullParserException, IOException {

        try{
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in,null);
            parser.nextTag();
            return readFeed(parser);
        }
        finally{
            in.close();
        }
    }

    //Gets parsed data from Placemark parser and Style parser
    private SongMap readFeed(XmlPullParser parser) throws XmlPullParserException,IOException{

        SongMap parsedMap = null;
        List<Placemark> placemarks = new ArrayList<>();

        parser.require(XmlPullParser.START_TAG,ns,"kml");
        // parser.next();

        while (parser.next() != XmlPullParser.END_TAG){

            System.out.println(parser.getName());
            if(parser.getEventType() != XmlPullParser.START_TAG){
                continue;
            }

            System.out.println(parser.getName());
            if(parser.getName().equals("Document")){
                parsedMap = readDocument(parser);
            }
        }

        return parsedMap;
    }


    private SongMap readDocument(XmlPullParser parser) throws IOException,XmlPullParserException{

        List<Placemark> placemarks = new ArrayList<>();
        List<Style> styles = new ArrayList<>();

        StyleXmlParser styleParser = new StyleXmlParser();
        PlacemarkXmlParser placemarkParser = new PlacemarkXmlParser();

        parser.require(XmlPullParser.START_TAG,ns,"Document");

        while (parser.next() != XmlPullParser.END_TAG){
            if(parser.getEventType() != XmlPullParser.START_TAG){
                continue;
            }
            String name = parser.getName();

            if(name.equals("Placemark")){
                placemarks.add(placemarkParser.readPlacemark(parser));
            } else if (name.equals("Style")) {
                styles.add(styleParser.readStyle(parser));
            } else {
//                CommonXmlParser.skip(parser);
            }
        }
        return new SongMap(placemarks, styles);
    }
}
