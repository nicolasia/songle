package com.example.nikii.songle;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Map;

public class CompletedSongsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_completed_songs);
       // addSongName();
        // System.out.println(getCompletedSongs());
        getCompletedSongs();
    }

    //Get saved data on SharedPreferences
    private void getCompletedSongs() {

        //Songs completed = null;
        ArrayList<String> completedSongNames = new ArrayList<>();
        final TextView myText = (TextView)findViewById(R.id.addSongName);
        SharedPreferences prefs = getSharedPreferences("Song", MODE_PRIVATE);
       // myText.append("hello");
        Map<String, ?> allEntries = prefs.getAll();

        if (prefs.getAll() != null) {
            for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
                 Log.d("SongName", entry.getKey() + ": " + entry.getValue());
                // data.completedSongs.addSong(data.songs.getSong((Integer) entry.getValue() - 1));
                completedSongNames.add(entry.getKey());
                myText.append(entry.getKey() + "\n");
                Log.e("Song Name", entry.getKey());
                //  Log.e("Integer in list", completedSongID + "");
                //   System.out.println(data.songs.getSong((Integer) entry.getValue() - 1).title);
                // Log.e("SongID", songID + "");
            }
        }
    }

    public void onClickToHome(View view){
        Intent intent = new Intent(this, HomePageActivity.class);
        startActivity(intent);
    }

    //Asks the user if they are sure they want to delete their progress
    public void onClickdeleteData(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to delete all your songs?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deleteData();
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        builder.create();
        builder.show();
    }

    public void deleteData(){

        SharedPreferences.Editor editor = getSharedPreferences("Song",MODE_PRIVATE).edit();

        editor.clear();
        editor.apply();
        Log.e("Deleted","Deleted");
    }

/*    private void addSongName(){
        TextView textView = (TextView)findViewById(R.id.addSongName);

        for(String songName : getCompletedSongs()){
            textView.setText(songName);
        }
    }*/
}
