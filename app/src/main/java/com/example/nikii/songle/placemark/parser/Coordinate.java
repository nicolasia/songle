package com.example.nikii.songle.placemark.parser;

/**
 * Created by nikii on 05/11/2017.
 */

public class Coordinate {

    public double longitude;
    public double latitude;
    public double altitude;

    //Constructs the parts needed for coordinates
    public Coordinate(double longitude,double latitude,double altitude) {
        this.longitude = longitude;
        this.latitude =latitude;
        this.altitude = altitude;
    }
}
