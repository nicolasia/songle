package com.example.nikii.songle.style.parser;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.example.nikii.songle.utils.Common;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by nicole on 16/11/2017.
 */

public class BitMapDownloader extends AsyncTask<List<Style>, Void, Integer> {
    private AsyncBitmapDownloader asyncBitmapDownloader;

    public BitMapDownloader(AsyncBitmapDownloader asyncBitmapDownloader) {
        this.asyncBitmapDownloader = asyncBitmapDownloader;
    }

    @Override
    protected Integer doInBackground(List<Style>... styles){
        try{
            for (Style style: styles[0]){
                Bitmap bitmap = loadBitmapFromNetwork(style.iconStyle.icon.href);
                style.setBitmap(bitmap);
                Log.e("bitmapdownloader", "one more style");
            }
            return styles[0].size();
        } catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Integer style){
        super.onPostExecute(style);

        if (asyncBitmapDownloader != null){
            asyncBitmapDownloader.onBitmapDownloaded (style);
        }
    }

    private Bitmap loadBitmapFromNetwork (String url) throws IOException {
        InputStream stream = null;
        Bitmap bitmap = null;

        try{
            stream = Common.downloadUrl(url);
            bitmap = BitmapFactory.decodeStream(stream);
        } finally{
            if (stream != null){
                stream.close();
            }
        }
        return bitmap;
    }

    public interface AsyncBitmapDownloader {
        void onBitmapDownloaded (Integer style);
    }
}
