package com.example.nikii.songle;

import com.example.nikii.songle.lyrics.parser.Lyrics;
import com.example.nikii.songle.songmap.parser.SongMap;
import com.example.nikii.songle.songs.parser.Song;
import com.example.nikii.songle.songs.parser.Songs;

/**
 * Created by nicole on 30/11/2017.
 */

//singleton class to store current progress

public class GamePlayData {


    public Songs songs;
    public Lyrics lyrics;
    public SongMap songMap;
    public Song currentSong;
    public int currentLevel;
    public int lastWordFoundIndex;

    private static final GamePlayData instance = new GamePlayData();

    //private constructor to avoid client applications to use constructor
    private GamePlayData(){}

    public static GamePlayData getInstance(){
        return instance;
    }
}