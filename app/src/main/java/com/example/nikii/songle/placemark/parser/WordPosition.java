package com.example.nikii.songle.placemark.parser;

/**
 * Created by nicole on 03/12/2017.
 */

public class WordPosition {
    private int row;
    private int column;
    private String word;

    public WordPosition(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
