package com.example.nikii.songle.style.parser;

import com.example.nikii.songle.utils.CommonXmlParser;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created by nikii on 05/11/2017.
 */

public class StyleXmlParser {

    private static final String ns = null;

    public Style readStyle(XmlPullParser parser) throws XmlPullParserException,IOException{

        parser.require(XmlPullParser.START_TAG,ns,"Style");
        String styleId = null;

        IconStyle iconStyle = null;

        styleId = readStyleId(parser);

        while(parser.next() != XmlPullParser.END_TAG){

            if(parser.getEventType() != XmlPullParser.START_TAG)
                continue;

            String name = parser.getName();

            if(name.equals("IconStyle")) {
                iconStyle = readIconStyle(parser);
            } else{
                CommonXmlParser.skip(parser);
            }

        }
        return new Style(styleId, iconStyle);
    }

    private IconStyle readIconStyle(XmlPullParser parser) throws XmlPullParserException,IOException{

        parser.require(XmlPullParser.START_TAG,ns,"IconStyle");
        double scale = 0;
        Icon icon = null;

        while(parser.next() != XmlPullParser.END_TAG){

            if(parser.getEventType() != XmlPullParser.START_TAG)
                continue;
            String name = parser.getName();

            if(name.equals("scale")) {
                scale = readScale(parser);
            } else if (name.equals("Icon")) {
                icon = readIcon(parser);
            } else{
                CommonXmlParser.skip(parser);
            }

        }
        return new IconStyle(scale, icon);
    }

    private Icon readIcon(XmlPullParser parser) throws XmlPullParserException,IOException{

        parser.require(XmlPullParser.START_TAG,ns,"Icon");
        String href = null;

        while(parser.next() != XmlPullParser.END_TAG){

            if(parser.getEventType() != XmlPullParser.START_TAG)
                continue;
            String name = parser.getName();

            if(name.equals("href")) {
                href = readLink(parser);
            } else{
                CommonXmlParser.skip(parser);
            }

        }
        return new Icon(href);
    }

    private String readStyleId(XmlPullParser parser) throws IOException, XmlPullParserException {
        //String styleId = "";
        parser.require(XmlPullParser.START_TAG, ns, "Style");
        String tag = parser.getName();
        String id = null;
        if (tag.equals("Style")){
            id = parser.getAttributeValue(null, "id");
//            parser.nextTag();
        }
//        parser.require(XmlPullParser.END_TAG, ns, "Style");
        return id;
    }

    private double readScale(XmlPullParser parser) throws IOException, XmlPullParserException{

        parser.require(XmlPullParser.START_TAG,ns,"scale");
        double scale = CommonXmlParser.readDouble(parser);
        parser.require(XmlPullParser.END_TAG,ns,"scale");
        return scale;
    }

    private String readLink(XmlPullParser parser) throws IOException, XmlPullParserException{

        parser.require(XmlPullParser.START_TAG,ns,"href");
        String href = CommonXmlParser.readText(parser);
        parser.require(XmlPullParser.END_TAG,ns,"href");
        return href;
    }
}
