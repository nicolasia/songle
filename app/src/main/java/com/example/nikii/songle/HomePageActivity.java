package com.example.nikii.songle;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.example.nikii.songle.lyrics.parser.Line;
import com.example.nikii.songle.lyrics.parser.Lyrics;
import com.example.nikii.songle.lyrics.parser.LyricsDownloader;
import com.example.nikii.songle.style.parser.BitMapDownloader;
import com.example.nikii.songle.songmap.parser.DownloadSongMapXml;
import com.example.nikii.songle.placemark.parser.Placemark;
import com.example.nikii.songle.songmap.parser.SongMap;
import com.example.nikii.songle.style.parser.Style;
import com.example.nikii.songle.songs.parser.DownloadSongsXml;
import com.example.nikii.songle.songs.parser.Song;
import com.example.nikii.songle.songs.parser.Songs;

import java.util.ArrayList;

public class HomePageActivity extends AppCompatActivity implements DownloadSongsXml.AsyncSongsDownloader,
        DownloadSongMapXml.AsyncSongMapDownloader, BitMapDownloader.AsyncBitmapDownloader,
        LyricsDownloader.AsyncLyricsDownloader{
    private final String Tag = "HomePageActivity";
    private final int MY_PERMISSIONS_REQUEST = 1;
    private ImageView bitmapView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        ImageButton exitButton = findViewById(R.id.button_exit);
        ImageButton playButton = findViewById(R.id.button_play);
        bitmapView = findViewById(R.id.bitmap);


    }

    //Asks user for permission to access their location
    private void askPermission(){
        int accessFineLocationGranted = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);

        ArrayList<String> permissionList = new ArrayList<>();

        if (accessFineLocationGranted != PackageManager.PERMISSION_GRANTED) {
            permissionList.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }

        if (permissionList.isEmpty()) return;

        ActivityCompat.requestPermissions(this,
                permissionList.toArray(new String[0]), MY_PERMISSIONS_REQUEST);
    }


    public void onClickExit(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to exit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        builder.create();
        builder.show();

    }

    @Override
    protected void onResume() {
        super.onResume();
        askPermission();
    }

    public void onClickPlay(View view){
       Intent intent = new Intent(this, LevelsActivity.class);
       startActivity(intent);
    }

    public void onClickInfo(View view){
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);

    }


    @Override
    public void onXmlDownloaded(Songs songs) {
        for ( Song s: songs.getSongs()){
            System.out.println(s.title);
        }

        GamePlayData data = GamePlayData.getInstance();
        data.songs = songs;
    }

    private SongMap m_songMap;
    @Override
    public void onSongMapDownloaded(SongMap songMap) {
        m_songMap = songMap;
        for ( Placemark p: songMap.getPlacemarks()){
            System.out.println(p.coord.toString());
        }

        for ( Style s: songMap.getStyles()){
            System.out.println(s.id);
        }

        new BitMapDownloader(this).execute(songMap.getStyles());
    }

    @Override
    public void onBitmapDownloaded(Integer bitmap) {
        for (Style s: m_songMap.getStyles()){
            System.out.println(s.getBitmap().toString());
        }
        bitmapView.setImageBitmap(m_songMap.getStyles().get(0).getBitmap());
    }

    @Override
    public void onLyricsDownloaded(Lyrics lyrics) {
        for (Line line: lyrics.getLines()){
            System.out.println(line.toString());
        }
    }
}
