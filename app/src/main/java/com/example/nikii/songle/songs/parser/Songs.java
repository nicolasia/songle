package com.example.nikii.songle.songs.parser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nicole on 30/11/2017.
 */

public class Songs{

    //Essentially just a list of song
    private List<Song> songs;

    public Songs(List<Song> songs) {
        this.songs = new ArrayList<>(songs);
    }

    public Songs(Songs songs) {
        this.songs = new ArrayList<>(songs.getSongs());
    }

    public Song getSong(int index){
        return songs.get(index);
    }

    public int getSongCount(){
        return songs.size();
    }

    public List<Song> getSongs() {
        return songs;
    }

    public void addSong(Song song){ songs.add(song);}

    public Song getSongFromTitle(String title){
        Song retSong = null;

        for (Song song: songs){
            if (title.equals(song.title)){
                retSong = song;
                break;
            }
        }

        return retSong;
    }
}
