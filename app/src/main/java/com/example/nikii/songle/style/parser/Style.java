package com.example.nikii.songle.style.parser;

import android.graphics.Bitmap;

/**
 * Created by nikii on 05/11/2017.
 */

public class Style {

    public final String id;
    public final IconStyle iconStyle;
    public Bitmap bitmap;

    public Style(String id, IconStyle iconStyle) {
        this.id = id;
        this.iconStyle = iconStyle;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
}
