package com.example.nikii.songle.songmap.parser;

import com.example.nikii.songle.placemark.parser.Placemark;
import com.example.nikii.songle.style.parser.Style;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nicole on 15/11/2017.
 */

public class SongMap {
    // All existing placemarks within the map
    private List<Placemark> placemarks;
    // All of the styles
    private List<Style> styles;

    /**
     * Normal contructor.
     * @param placemarks
     * @param styles
     */
    public SongMap(List<Placemark> placemarks, List<Style> styles) {
        this.placemarks = new ArrayList<>(placemarks);
        this.styles = new ArrayList<>(styles);
    }

    /**
     * Copy constructor is used to clone the already existing object.
     * @param copy
     */
    public SongMap (SongMap copy) {
        this.placemarks = new ArrayList<>(copy.placemarks);
        this.styles = new ArrayList<>(copy.styles);
    }

    public List<Placemark> getPlacemarks() {
        return placemarks;
    }

    public List<Style> getStyles() {
        return styles;
    }
}
