package com.example.nikii.songle.placemark.parser;

/**
 * Created by nikii on 03/11/2017.
 */

public class Placemark {
    public final WordPosition wordPosition;
    public final String description;
    public final String styleURL;
    public final Coordinate coord;
    private boolean isFound;

    public Placemark(WordPosition wordPosition, String description, String styleURL, Coordinate coord){

        this.wordPosition = wordPosition;
        this.description = description;
        this.styleURL = styleURL;
        this.coord = coord;
    }

    //Checks if a place mark has been collected
    public boolean isFound() {
        return isFound;
    }

    public void setFound(boolean found) {
        isFound = found;
    }
}
