package com.example.nikii.songle;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.nikii.songle.lyrics.parser.Lyrics;
import com.example.nikii.songle.lyrics.parser.LyricsDownloader;
import com.example.nikii.songle.style.parser.BitMapDownloader;
import com.example.nikii.songle.songmap.parser.DownloadSongMapXml;
import com.example.nikii.songle.songmap.parser.SongMap;
import com.example.nikii.songle.songs.parser.Song;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 ToDo:
 get songs that were not played and then randomize that set of songs
 Check that the user still has internet connection to download the maps
 Give time out error if the device takes more than 20s to download the maps
 */
public class SplashScreenTwo extends AppCompatActivity implements DownloadSongMapXml.AsyncSongMapDownloader,
        BitMapDownloader.AsyncBitmapDownloader, LyricsDownloader.AsyncLyricsDownloader{

    public final static String MAP_LEVEL = "MAP_LEVEL";
    /** Duration of wait **/
    private final int SPLASH_DISPLAY_LENGTH = 3000;
    private int mapLevel;
    private boolean isDownloadComplete;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_splashscreen);

        mapLevel = getIntent().getIntExtra(LevelsActivity.GAME_LEVEL, 0);
        Boolean isLevelInitialised = getIntent().getBooleanExtra(MainGameActivity.CURRENT_LEVEL,false);
        /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        if(isNetworkConnected()){
            if(checkIfAllPlayed() == 1) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                /* Create an Intent that will start the Menu-Activity. */

                        Intent mainIntent = new Intent(SplashScreenTwo.this, MainGameActivity.class);
                        mainIntent.putExtra(MAP_LEVEL, mapLevel);
                        startActivity(mainIntent);
                        finish();
                    }
                }, SPLASH_DISPLAY_LENGTH);
            } else{
                finish();
                backToHome();
            }
        } else{
            Toast.makeText(this, "You need internet to play this game! Please retry", Toast.LENGTH_LONG).show();
            finish();
        }


        //Stores current level in the singleton
        GamePlayData data = GamePlayData.getInstance();
        if (isLevelInitialised){
            mapLevel = data.currentLevel;
        }
        else{
            mapLevel = mapLevel = getIntent().getIntExtra(LevelsActivity.GAME_LEVEL, 0);
        }
        Log.e("chosen map level", mapLevel + "");

    }

    @Override
    protected void onResume() {
        super.onResume();
/*        Log.e("retrieved data", retrieveData()+ "");
        Log.e("data size", retrieveData().size() + "");
        Log.e("checkRepeated", checkSongsRepeated() + "");
        Log.e("RandomSong", getRandomSong() + "");*/
       // deleteData();
        downloadMapData();
    }

    //Generates a random song from the song list.
    private int getRandomSong(){

        GamePlayData gameData = GamePlayData.getInstance();
        String songToPlayTitle = "";
        Random random = new Random();

        List<String> songs = getNotCompletedSongs();
        int songCount = songs.size();
        int min = 0;

        if(songCount > 0){
            int randomIndex = random.nextInt(songCount - min) + min;
            songToPlayTitle = songs.get(randomIndex);
            Log.e("randomSong" , songToPlayTitle +"");
            gameData.currentSong = gameData.songs.getSongFromTitle(songToPlayTitle);
        } else {
           // backToHome();
        }
        return gameData.currentSong.number;
    }

    private void backToHome(){
        Intent intent = new Intent(this,HomePageActivity.class);
        startActivity(intent);
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    //This gets a list of songs not yet completed by the player. This ensures that a song won't be repeated
    private List<String> getNotCompletedSongs(){

        List<String> notCompletedSongs = new ArrayList<>();
//        List<Integer> newSongNumberList = new ArrayList<>();
        GamePlayData data = GamePlayData.getInstance();

        for (Song s : data.songs.getSongs()) {
            notCompletedSongs.add(s.title);
        }
        Log.e("allTitles", notCompletedSongs + "");

        notCompletedSongs.removeAll(getCompletedSongs());

        Log.e("New List", notCompletedSongs + "");
        return notCompletedSongs;
    }

    public Collection<String> getCompletedSongs(){

        //Songs completed = null;
        Collection<String> completedSongNames = new ArrayList<>();
        GamePlayData data = GamePlayData.getInstance();

        SharedPreferences prefs = getSharedPreferences("Song", MODE_PRIVATE);

        Map<String, ?> allEntries = prefs.getAll();

        if (prefs.getAll() != null){
            for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
               // Log.d("SongName", entry.getKey() + ": " + entry.getValue());
                // data.completedSongs.addSong(data.songs.getSong((Integer) entry.getValue() - 1));
                completedSongNames.add(entry.getKey());
                Log.e("Integer in list", completedSongNames + "");
                //   System.out.println(data.songs.getSong((Integer) entry.getValue() - 1).title);
                // Log.e("SongID", songID + "");
            }
        }

        return completedSongNames;
        //  startActivity(intent);
    }

    //checks if all songs have been played or not
    private int checkIfAllPlayed(){
        if(getNotCompletedSongs().size() == 0){
            Toast.makeText(this,"You have completed all the songs! Restart your progress in settings", Toast.LENGTH_LONG).show();
            return 0;
        } else{
            return 1;
        }
    }

    //generates correct link from data and loads the correct map
    public void downloadMapData(){

        if(getNotCompletedSongs().size() == 0){
            //Toast.makeText(this,"You have completed all the songs! Restart your progress in settings", Toast.LENGTH_LONG).show();
            finish();
          //  backToHome();
        } else {
            String mapsLink;
            String lyricsLink;
            String formatted = String.format("%02d", getRandomSong());

            mapsLink = "http://www.inf.ed.ac.uk/teaching/courses/selp/data/songs/" + formatted + "/map" + mapLevel + ".kml";
            lyricsLink = "http://www.inf.ed.ac.uk/teaching/courses/selp/data/songs/" + formatted + "/words.txt";
            new DownloadSongMapXml(this).execute(mapsLink);
            new LyricsDownloader(this).execute(lyricsLink);
        }
    }

    public void deleteData(){

        SharedPreferences.Editor editor = getSharedPreferences("Song",MODE_PRIVATE).edit();

        editor.clear();
        editor.apply();
        Log.e("Deleted","Deleted");
    }


    //stores current progress into singleton
    @Override
    public void onSongMapDownloaded(SongMap songMap) {

        GamePlayData data = GamePlayData.getInstance();
        data.songMap = songMap;


        new BitMapDownloader(this).execute(songMap.getStyles());
    }


    @Override
    public void onBitmapDownloaded(Integer style) {
        isDownloadComplete = true;
        Log.i("SplashScreenTwo","downloaded " + style + " number");
    }

    @Override
    public void onLyricsDownloaded(Lyrics lyrics) {
        GamePlayData data = GamePlayData.getInstance();
        data.lyrics = lyrics;
    }
}
