package com.example.nikii.songle.style.parser;

/**
 * Created by nikii on 15/11/2017.
 */

public class Icon {
    public final String href;

    public Icon(String href) {
        this.href = href;
    }
}
