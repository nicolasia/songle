package com.example.nikii.songle.utils;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created by nikii on 15/11/2017.
 */

public class CommonXmlParser {
    public static void skip (XmlPullParser parser) throws XmlPullParserException,IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG){
            throw new IllegalStateException();
        }
        int depth = 1;
        while(depth != 0){
            switch(parser.next()){
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }

    public static String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if(parser.next() == XmlPullParser.TEXT){
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    public static double readDouble(XmlPullParser parser) throws IOException, XmlPullParserException {
        double result = 0;
        if(parser.next() == XmlPullParser.TEXT){
            try {
                result = Double.parseDouble(parser.getText());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            parser.nextTag();
        }
        return result;
    }
}
