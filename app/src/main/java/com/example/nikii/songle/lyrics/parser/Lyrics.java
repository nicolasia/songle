package com.example.nikii.songle.lyrics.parser;

import android.text.TextUtils;
import android.util.SparseArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by nicole on 16/11/2017.
 */

public class Lyrics {
    private List<Line> lines;

    public Lyrics() {
        this.lines = new ArrayList<>();
    }

    //Sets lyrics to be a list of lines
    public Lyrics(List<Line> lines){
        this.lines = new ArrayList<>();
        for (Line line: lines){
            this.lines.add(new Line(line));
        }
    }

    //Gets the collected words and puts in in the right place for the review words layout
    public Lyrics createCollectable(){
        Lyrics collectable = new Lyrics(lines);

        for (Line line: collectable.getLines()){
            for (int i = 0; i < line.getLineLength(); i++){
                line.setWord(i, line.getWord(i).replaceAll("[A-Za-z0-9]", "_"));
            }
        }

        return collectable;
    }

    public void appendLine (Line line){
        this.lines.add(line);
    }

    public void removeLine(int row){
        this.lines.remove(row);
    }

    public String getWord(int row, int column){
        String word = null;

        // firstly make sure that such row exist
        if (this.lines.get(row) != null){
            word = this.lines.get(row).getWord(column);
        }
        return word;
    }

    public void setWord(int row, int column, String word){
        // firstly make sure that such row exist
        if (lines.get(row) != null){
            lines.get(row).setWord(column, word);
        }
    }

    public int getLineCount(){
        return this.lines.size();
    }

    public Line getLine(int row){
        return this.lines.get(row);
    }

    public List<Line> getLines(){
        return  lines;
    }

    @Override
    public String toString() {
        List<String> listOfLines = new ArrayList<>();

        for (Line line: lines){
            listOfLines.add(line.toString());
        }

        return TextUtils.join("\n", listOfLines);
    }
}
